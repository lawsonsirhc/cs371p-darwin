// ----------------
// RunDarwin.cpp
// ----------------

// --------
// includes
// --------

#include <sstream>  // istringstream
#include <iostream> // cin, cout
#include <list>     // list
#include <cassert>  // assert
#include <cstdlib>  // rand
#include <vector>   // vector
#include "Darwin.hpp"

// ------------
// debug macros
// ------------
#define DEBUG_RUN 0

// -------
// pragmas
// -------

#ifdef __clang__
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

// ----
// main
// ----

int main () {
    using namespace std;
    string s;
    // Read in the first line that contains the number of Tests
    getline(cin, s);

    istringstream iss(s);
    int numTests;
    iss >> numTests;
    if(DEBUG_RUN)
        cout << "numTests: " << numTests << endl << endl;

    assert(1 <= numTests && numTests <= 200);

    getline(cin, s);

    // Now to read in the information necessary for each test
    for(int i = 0; i < numTests; ++i) {
        string temp;
        // Resetting srand for each test.
        srand(0);
        getline(cin, temp);
        istringstream iss(temp);
        int row;
        int col;
        iss >> row;
        iss >> col;
        assert(1 <= row && row <= 200);
        assert(1 <= col && col <= 200);

        if(DEBUG_RUN) {
            cout << "Row: " << row << " Col: " << col << endl;
        }

        // Darwin constructor to specify the row and column size
        // of the internal grid, this method also constructs the 4 Species objects
        // to be used by all creatures.
        Darwin darwin(row, col);

        getline(cin, temp);
        istringstream iss_1(temp);
        int nLines;
        iss_1 >> nLines;
        assert(1 <= nLines && nLines <= 200);

        if(DEBUG_RUN) {
            cout << "Num lines: " << nLines << endl;
        }

        // This was my way of ensuring that creatures were not instantly deleted
        // Do not make use of copy elision but understand it would be an
        // optimization that I could make.
        vector<Creature> all_creatures(nLines);

        for(int j = 0; j < nLines; ++j) {
            string line;
            getline(cin, line);
            istringstream iss(line);

            char type;
            int  pos_r;
            int  pos_c;
            char dir;
            iss >> type;
            iss >> pos_r;
            iss >> pos_c;
            iss >> dir;

            assert(type == 'f' ||
                   type == 'h' ||
                   type == 'r' ||
                   type == 't');
            assert(0 <= pos_r && pos_r < row);
            assert(0 <= pos_c && pos_c < col);
            assert(dir == 'n' ||
                   dir == 's' ||
                   dir == 'e' ||
                   dir == 'w');

            Direction d;
            switch(dir) {
            case 'n':
                d = Direction::North;
                break;
            case 's':
                d = Direction::South;
                break;
            case 'e':
                d = Direction::East;
                break;
            case 'w':
                d = Direction::West;
                break;
            default:
                break;
            }

            // I have the constructor such that it takes in a species pointer
            // Direction and type which is a character that represents a species.
            all_creatures[j] = {darwin.getSpecies(type), d, type};

            // Making sure to insert the creature into the grid and into Darwin's own
            // list of creatures (will help with multiple iterations)
            darwin.insertCreature(&all_creatures[j], pos_r, pos_c);
        }

        if(DEBUG_RUN) {
            cout << "Printing all creatures" << endl;
            for(int j = 0; j < (int)all_creatures.size(); ++j) {
                cout << "Creature: " << j << endl;
                all_creatures[j].printCreature();
                cout << endl;
            }
        }

        getline(cin, temp);
        istringstream iss_2(temp);

        // Next get the number of simulations we want to run
        // and how frequently we output.
        int sim;
        int output;

        // Darwin will alway print the initial state.
        cout << "*** Darwin " << row << "x" << col << " ***" << endl;
        cout << "Turn = 0." << endl;

        // Will iterate through the grid in darwin to print out the creatures
        darwin.printState();
        cout << endl;
        cout << endl;
        iss_2 >> sim;
        iss_2 >> output;
        assert(1 <= sim && sim <= 2000);
        assert(1 <= output && output <= 200);

        for(int sim_num = 1; sim_num <= sim; ++sim_num) {
            // To make the code clean, we will just call
            darwin.oneIteration();

            if(sim_num % output == 0) {
                cout << "Turn = " << sim_num << "." << endl;
                darwin.printState();
                if((sim-sim_num) / output != 0) {
                    cout << endl;
                    cout << endl;
                }
            }
        }

        // Code to make sure that we don't print out an extra print line at the end
        // of execution
        if(i != numTests -1) {
            cout << endl;
            cout << endl;
            getline(cin, temp);
        }

        if(DEBUG_RUN) {
            cout << endl;
            cout << "FINISHED TEST: " << endl;
        }

    }

    return 0;
}