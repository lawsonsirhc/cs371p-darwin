#ifndef SPECIES_H // include guard
#define SPECIES_H

// --------
// Includes
// --------
#include "Instruction.hpp"
#include <iostream> // cin, cout
#include <vector>
#define DEBUG_SPECIES 0
using namespace std;

// -------
// Species
// -------
class Species {
private:
    // This will store the internal set of instructions
    // for this particular species
    vector<pair<Instruction, int>> _instructions;

public:
    // Will return the instruction given a program counter
    pair<Instruction, int> getInstruction(int pc);

    // Simply adds an instruction
    void addInstruction(Instruction i, int arg);

    // Printing out all of the instructions of this species
    // only for debugging purposes.
    void printInstructions();
};

#endif