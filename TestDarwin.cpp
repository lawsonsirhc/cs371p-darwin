// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// -------
// pragmas
// -------

#ifdef __clang__
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <string>    // string

#include <iostream>

#include "gtest/gtest.h"

#include "Darwin.hpp"
#include <vector>

TEST(DarwinFrontPosition, test0) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::North;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    pair<int, int> ans(-1, 0);
    pair<int, int> result = darwin.getFrontPosition(pos_r, pos_c);
    ASSERT_EQ(ans.first, result.first);
    ASSERT_EQ(ans.second, result.second);
}

TEST(DarwinFrontPosition, test1) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    pair<int, int> ans(1, 0);
    pair<int, int> result = darwin.getFrontPosition(pos_r, pos_c);
    ASSERT_EQ(ans.first, result.first);
    ASSERT_EQ(ans.second, result.second);
}

TEST(DarwinFrontPosition, test2) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::West;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    pair<int, int> ans(0, -1);
    pair<int, int> result = darwin.getFrontPosition(pos_r, pos_c);
    ASSERT_EQ(ans.first, result.first);
    ASSERT_EQ(ans.second, result.second);
}

TEST(DarwinFrontPosition, test3) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::East;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    pair<int, int> ans(0, 1);
    pair<int, int> result = darwin.getFrontPosition(pos_r, pos_c);
    ASSERT_EQ(ans.first, result.first);
    ASSERT_EQ(ans.second, result.second);
}

TEST(DarwinFrontCreature, test0) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::North;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    Creature* ans = nullptr;
    Creature* result = darwin.getFrontCreature(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFrontCreature, test1) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* ans = &all_creatures[1];
    Creature* result = darwin.getFrontCreature(0, 0);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFrontCreature, test2) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    Creature* ans = nullptr;
    Creature* result = darwin.getFrontCreature(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFrontCreature, test3) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::East;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 0;
    pos_c = 1;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* ans = &all_creatures[1];
    Creature* result = darwin.getFrontCreature(0, 0);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFacingWall, test0) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::North;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    bool ans = true;
    bool result = darwin.isFacingWall(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFacingWall, test1) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    bool ans = false;
    bool result = darwin.isFacingWall(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFacingWall, test2) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::West;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    bool ans = true;
    bool result = darwin.isFacingWall(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinFacingWall, test3) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::East;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);
    bool ans = false;
    bool result = darwin.isFacingWall(pos_r, pos_c);
    ASSERT_EQ(ans, result);
}

TEST(DarwinOneIteration, test0) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::East;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'f';
    d = Direction::East;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    darwin.oneIteration();

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = nullptr;
    ASSERT_EQ(ans, result);
}

TEST(DarwinOneIteration, test1) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::West;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'f';
    d = Direction::East;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    darwin.oneIteration();

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = &all_creatures[0];
    ASSERT_EQ(ans, result);
}

TEST(DarwinOneIteration, test2) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::North;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'f';
    d = Direction::East;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    darwin.oneIteration();

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = &all_creatures[0];
    ASSERT_EQ(ans, result);
}

TEST(DarwinOneIteration, test4) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'f';
    d = Direction::East;
    pos_r = 2;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    darwin.oneIteration();

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = &all_creatures[1];
    ASSERT_EQ(ans, result);
}

TEST(SpeciesGetInstruction, test0) {
    Darwin darwin(10, 10);

    Species* food = darwin.getSpecies('r');

    pair<Instruction, int> inst = food->getInstruction(1);
    Instruction ans = Instruction::if_empty;
    ASSERT_EQ(inst.first, ans);
    int ans_2 = 7;
    ASSERT_EQ(inst.second, ans_2);
}

TEST(SpeciesGetInstruction, test1) {
    Darwin darwin(10, 10);

    Species* food = darwin.getSpecies('f');

    pair<Instruction, int> inst = food->getInstruction(0);
    Instruction ans = Instruction::left;
    ASSERT_EQ(inst.first, ans);
    int ans_2 = -1;
    ASSERT_EQ(inst.second, ans_2);

}

TEST(SpeciesGetInstruction, test2) {
    Darwin darwin(10, 10);

    Species* food = darwin.getSpecies('r');

    pair<Instruction, int> inst = food->getInstruction(9);
    Instruction ans = Instruction::infect;
    ASSERT_EQ(inst.first, ans);
    int ans_2 = -1;
    ASSERT_EQ(inst.second, ans_2);
}

TEST(SpeciesGetInstruction, test3) {
    Darwin darwin(10, 10);

    Species* food = darwin.getSpecies('t');

    pair<Instruction, int> inst = food->getInstruction(2);
    Instruction ans = Instruction::go;
    ASSERT_EQ(inst.first, ans);
    int ans_2 = 0;
    ASSERT_EQ(inst.second, ans_2);
}

TEST(CreatureExecute, test0) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::West;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* in_front = darwin.getFrontCreature(0, 0);
    bool wall = darwin.isFacingWall(0, 0);
    bool result = all_creatures[0].execute(in_front, wall);

    bool ans = false;

    ASSERT_EQ(ans, result);
}


TEST(CreatureExecute, test1) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::East;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* in_front = darwin.getFrontCreature(0, 0);
    bool wall = darwin.isFacingWall(0, 0);
    bool result = all_creatures[0].execute(in_front, wall);

    bool ans = true;

    ASSERT_EQ(ans, result);
}
TEST(CreatureExecute, test2) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::North;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* in_front = darwin.getFrontCreature(0, 0);
    bool wall = darwin.isFacingWall(0, 0);
    all_creatures[0].execute(in_front, wall);

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = &all_creatures[0];

    ASSERT_EQ(ans, result);
}

TEST(CreatureExecute, test3) {
    Darwin darwin(10, 10);
    vector<Creature> all_creatures(4);
    char type = 'h';
    Direction d = Direction::South;
    int pos_r = 0;
    int pos_c = 0;

    all_creatures[0] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[0], pos_r, pos_c);

    type = 'h';
    d = Direction::North;
    pos_r = 1;
    pos_c = 0;

    all_creatures[1] = {darwin.getSpecies(type), d, type};
    darwin.insertCreature(&all_creatures[1], pos_r, pos_c);

    Creature* in_front = darwin.getFrontCreature(0, 0);
    bool wall = darwin.isFacingWall(0, 0);
    all_creatures[0].execute(in_front, wall);

    Creature* result = darwin.getFrontCreature(1, 0);
    Creature* ans = &all_creatures[0];

    ASSERT_EQ(ans, result);
}