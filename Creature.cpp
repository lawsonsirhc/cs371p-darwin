// --------
// Includes
// --------

#include "Creature.hpp"

Creature::Creature(Species* s, Direction d, char st) : _species(s), _direction(d), _species_symbol(st) {}

bool Creature::execute(Creature* inFront, bool wall) {
    if(DEBUG_CREATURE) {
        cout << "In execute" << endl;
        cout << "This creature's species instructions are: " << endl;
        _species->printInstructions();
        cout << "going to start at pc: " << _program_counter << endl;
    }

    // Here we need keep track of whether a creature has called an action
    // instruction because that means that you don't continue
    bool action = false;
    // Here this is what we need to return out depending on whether the hop gets called
    // or not.
    bool hoppCalled = false;

    // Reason for the do-while is because we know we must do at least one iteration, but we want
    // to make sure we can listen for an action being called.
    do {
        pair<Instruction, int> inst = _species->getInstruction(_program_counter);

        if(DEBUG_CREATURE) {
            cout << "Executing instruction: ";
            switch(inst.first) {
            case Instruction::hop:
                cout << "Hop" << endl;
                break;
            case Instruction::left:
                cout << "Left" << endl;
                break;
            case Instruction::right:
                cout << "Right" << endl;
                break;
            case Instruction::infect:
                cout << "Infect" << endl;
                break;
            case Instruction::if_enemy:
                cout << "If_enemy" << endl;
                break;
            case Instruction::if_empty:
                cout << "If_empty" << endl;
                break;
            case Instruction::if_random:
                cout << "If_random" << endl;
                break;
            case Instruction::go:
                cout << "Go" << endl;
                break;
            default:
                cout << "NONE GIVEN" << endl;
                break;
            }
        }

        if(inst.first == Instruction::hop) {
            if(!inFront && !wall) {
                hoppCalled = true;
            }
            action = true;
        } else if(inst.first == Instruction::left) {
            action = true;
        } else if(inst.first == Instruction::right) {
            action = true;
        } else if(inst.first == Instruction::infect) {
            action = true;
        } else {
            action = false;
        }
        // Because executeInstruction will return what the new pc should be, we need to save it.
        _program_counter = executeInstruction(inFront, inst, wall, _program_counter);
        if(DEBUG_CREATURE) {
            cout << "PC is now: " << _program_counter << endl;
        }
    } while(!action);

    return hoppCalled;
}

// A getter that will just return the direction, needed by Darwins getFront methods.
Direction Creature::getDirection() {
    return _direction;
}

// Used by printState
void Creature::printSymbol() {
    cout << _species_symbol;
}

// Debugging purposes
void Creature::printCreature() {
    cout << "Creature: ";
    cout << hex;
    cout << this;
    cout << dec;
    cout << endl;
    cout << "Species: ";
    cout << hex;
    cout << _species;
    cout << dec;
    cout << endl;
    cout << "Direction: ";
    switch(_direction) {
    case Direction::North:
        cout << "North" << endl;
        break;
    case Direction::South:
        cout << "South" << endl;
        break;
    case Direction::West:
        cout << "West" << endl;
        break;
    case Direction::East:
        cout << "East" << endl;
        break;
    default:
        cout << "ERROR IN CREATURE::printCreature" << endl;
        throw exception();
    }
    cout << "Program Counter: " << _program_counter << endl;
}

// Used to make sure creatures do not get executed more than once
// one iteration.
void Creature::setVisited() {
    _visited = 1;
}

// Used to make sure creatures do not get executed more than once
// one iteration.
void Creature::resetVisited() {
    _visited = 0;
}

// Used to make sure creatures do not get executed more than once
// one iteration.
int Creature::getVisited() {
    return _visited;
}

