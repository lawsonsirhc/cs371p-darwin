// --------
// Includes
// --------
#include "Species.hpp"

// Return the instruction at the given pc
pair<Instruction, int> Species::getInstruction(int pc) {
    return _instructions[pc];
}

// Will create an instruction and an argument, create the necessary pair
// and insert it into internal array
void Species::addInstruction(Instruction i, int arg) {
    pair<Instruction, int> inst;
    inst.first = i;
    inst.second = arg;

    _instructions.push_back(inst);
}

// Debugging purposes.
void Species::printInstructions() {
    for(int i = 0; i < (int)_instructions.size(); ++i) {
        cout << i << ": ";
        pair<Instruction, int> inst = _instructions[i];
        switch(inst.first) {
        case Instruction::hop:
            cout << "hop";
            break;
        case Instruction::left:
            cout << "left";
            break;
        case Instruction::right:
            cout << "right";
            break;
        case Instruction::infect:
            cout << "infect";
            break;
        case Instruction::if_empty:
            cout << "if_empty " << inst.second;
            break;
        case Instruction::if_wall:
            cout << "if_wall " << inst.second;
            break;
        case Instruction::if_random:
            cout << "if_random " << inst.second;
            break;
        case Instruction::if_enemy:
            cout << "if_enemy " << inst.second;
            break;
        case Instruction::go:
            cout << "go " << inst.second;
            break;
        default:
            cout << "species.hpp::printInstruction" << endl;
            throw exception();
        }
        cout << endl;
    }
}