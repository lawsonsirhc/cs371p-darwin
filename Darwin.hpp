#ifndef DARWIN_H // include guard
#define DARWIN_H

// -------------
// Darwin.hpp
// -------------
#include "Creature.hpp"
#include <vector>
#include <algorithm>
#include <iostream> // cin, cout

#define DEBUG_DARWIN 0

// ------------------
// Darwin board class
// ------------------

class Darwin {
private:
    // Will keep track of references to the only Species
    // in the existance of this Darwin Object
    vector<Species> _species;

    // Will keep track of where the creatures are in the
    // board
    vector<vector<Creature*>> _grid;

    // Will keep track of the creature objects themselves
    // This will help of keeping track which creatures have
    // already been executed (no double dipping)
    vector<Creature*> _creature;
public:

    // Will create all Species that are going to be used throughout
    // this darwin object
    void createSpecies();

    // Returns a pair that represents the front position of the given cordinates
    // DOES NOT ACCOUNT FOR OUT OF BOUNDS POINTS
    pair<int, int> getFrontPosition(int row, int col);

    // Returns the pointer of the creature that is in front
    // of the given cordinate.
    // if there is nothing in front or if the current creature is
    // facing a wall it will return nullptr
    Creature* getFrontCreature(int row, int col);

    // Returns true if the current creature is facing a wall
    bool isFacingWall(int row, int col);

    // Constructs the board that creatures will play on
    // specifies the length and width. Will also create the
    // species objects.
    Darwin(int row_len, int col_len);

    // Returns the pointer to the species that a creature will
    // set itself to.
    Species* getSpecies(char type);

    // Given a creature and a cordinate, we will insert the creature
    // at that position into the internal grid.
    void insertCreature(Creature* c, int row, int col);

    // Will print out the board (specifically the creature pointers)
    // Used for Debugging
    void printBoard();

    // Will print out the board, to the specifics of the test
    void printState();

    // Will execute one iteration with the current state of the board
    void oneIteration();

};

#endif