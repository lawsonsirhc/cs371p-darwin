#ifndef CREATURE_H // include guard
#define CREATURE_H

// --------
// Includes
// --------
#include "Direction.hpp"
#include "Species.hpp"
#include <iostream> // cin, cout
using namespace std;
#define DEBUG_CREATURE 0

// --------
// Creature
// --------

class Creature {
private:
    // Holds the pointer to this creatures species
    Species* _species;

    // Holds the current direction
    Direction _direction;

    // Holds the character that represents this creatures
    // species. (Used for printing)
    char _species_symbol;

    // Current program counter
    int _program_counter = 0;

    // This is used to check whether this creature has been visited
    // during this current iteration of execution.
    int _visited = 0;

    // Given the information of the creature in front of this creature, what
    // instruction to execute, and whether it is in front of a wall or not
    // it will properly alter this creature

    // This method is only in charge of taking care of business within the creature,
    // and nothing outside of it.

    // Will return what the new program counter should be.
    int executeInstruction(Creature* inFront, pair<Instruction, int> inst, bool wall, int pc) {
        if(DEBUG_CREATURE) {
            cout << "Inside executeInstruction" << endl;
        }

        Instruction inst_type = inst.first;
        int line = inst.second;

        // Because this function depends on which instruction is called
        // we need the switch case.
        switch(inst_type) {
        // For example, because hop does not alter this creatures object,
        // nothing other than increasing the program count is changed.
        case Instruction::hop:
            return pc + 1;
        // For left, we do need to change contents within the creature, so we do more than just
        // increase the program count.
        case Instruction::left:
            switch(_direction) {
            case Direction::North:
                _direction = Direction::West;
                break;
            case Direction::West:
                _direction = Direction::South;
                break;
            case Direction::South:
                _direction = Direction::East;
                break;
            case Direction::East:
                _direction = Direction::North;
                break;
            default:
                cout << "executeInstruction left" << endl;
                throw exception();
            }
            return pc + 1;
        case Instruction::right:
            switch(_direction) {
            case Direction::North:
                _direction = Direction::East;
                break;
            case Direction::East:
                _direction = Direction::South;
                break;
            case Direction::South:
                _direction = Direction::West;
                break;
            case Direction::West:
                _direction = Direction::North;
                break;
            default:
                cout << "executeInstruction right" << endl;
                throw exception();
            }
            return pc + 1;
        case Instruction::infect:
            // For infect we need to make sure that we change the species
            // of our infront creature, the corresponding symbol and the program count
            inFront->_species = _species;
            inFront->_species_symbol = _species_symbol;
            inFront->_program_counter = 0;
            return pc + 1;
        case Instruction::if_empty:
            if(!inFront && !wall) {
                return line;
            }
            return pc + 1;
        case Instruction::if_random:
        {
            int temp = rand();
            if(temp % 2 == 1) {
                return line;
            }
            return pc + 1;
        }
        case Instruction::if_enemy:
            if(inFront && inFront->_species != _species) {
                return line;
            }
            return pc + 1;
        case Instruction::go:
            return line;
        default:
            cout << "executeInstruction" << endl;
            throw exception();
        }
    }

public:

    Creature() = default;

    // Will create the creature in acordance to the arguments
    Creature(Species* s, Direction d, char st);

    // Will take in a creature that represents what is infront and whether it is
    // facing a wall or not.

    // will return a boolean to represent that the board must be changed
    bool execute(Creature* inFront, bool wall);

    // Will return the direction of this creature
    // is important for Darwin's getFront methods
    Direction getDirection();

    // Will print the symbol of this creature
    // used for printing the state of the board.
    void printSymbol();

    // Will print out this creature (used for debugging purposes)
    void printCreature();

    // This method will be used to check whether a particular
    // creature has already been executed in a particular iteration
    void setVisited();

    // Will set the visited to 0, indicating it is not visited
    void resetVisited();

    // Will be used to see if the creature has already been visited
    int getVisited();
};

#endif