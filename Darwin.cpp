// --------
// Includes
// --------
#include "Darwin.hpp"

// Will set up all species instructions that we care about.
void Darwin::createSpecies() {
    if(DEBUG_DARWIN) {
        cout << "Species.size(): " << _species.size() << endl << endl;
    }

    // Instructions are set up as follows:
    // Instruction - Will tell you which instruction you are using
    // int         - Will tell you the associated number for the instruction (if none needed -1)

    // Constructing the Food:
    _species[0].addInstruction(Instruction::left, -1);
    _species[0].addInstruction(Instruction::go, 0);

    // Constructing the Hopper:
    _species[1].addInstruction(Instruction::hop, -1);
    _species[1].addInstruction(Instruction::go, 0);

    // Constructing the Rover:
    _species[2].addInstruction( Instruction::if_enemy,  9);
    _species[2].addInstruction( Instruction::if_empty,  7);
    _species[2].addInstruction(Instruction::if_random,  5);
    _species[2].addInstruction(     Instruction::left, -1);
    _species[2].addInstruction(       Instruction::go,  0);
    _species[2].addInstruction(    Instruction::right, -1);
    _species[2].addInstruction(       Instruction::go,  0);
    _species[2].addInstruction(      Instruction::hop, -1);
    _species[2].addInstruction(       Instruction::go,  0);
    _species[2].addInstruction(   Instruction::infect, -1);
    _species[2].addInstruction(       Instruction::go,  0);

    // Constructing the Trap:
    _species[3].addInstruction( Instruction::if_enemy,  3);
    _species[3].addInstruction(     Instruction::left, -1);
    _species[3].addInstruction(       Instruction::go,  0);
    _species[3].addInstruction(   Instruction::infect, -1);
    _species[3].addInstruction(       Instruction::go,  0);

    // to print out that the addresses of the species created
    if(DEBUG_DARWIN) {
        for(int j = 0; j < 4; ++j) {
            switch(j) {
            case 0:
                cout << "Food: ";
                cout << hex;
                cout << &_species[0];
                cout << dec;
                cout << endl;
                break;
            case 1:
                cout << "Hopper: ";
                cout << hex;
                cout << &_species[1];
                cout << dec;
                cout << endl;
                break;
            case 2:
                cout << "Rover: ";
                cout << hex;
                cout << &_species[2];
                cout << dec;
                cout << endl;
                break;
            case 3:
                cout << "Trap: ";
                cout << hex;
                cout << &_species[3];
                cout << dec;
                cout << endl;
                break;

            }
            _species[j].printInstructions();
            cout << endl;
        }
    }
}

pair<int, int> Darwin::getFrontPosition(int row, int col) {
    Creature* cur_creature = _grid[row][col];
    Direction cur_direction = cur_creature->getDirection();

    // Here it's just a matter of where the direction of the creature.
    // so it's just a matter of doing the correct position.
    int new_row = row;
    int new_col = col;
    switch(cur_direction) {
    case Direction::North:
        --new_row;
        break;
    case Direction::South:
        ++new_row;
        break;
    case Direction::West:
        --new_col;
        break;
    case Direction::East:
        ++new_col;
        break;
    default:
        cout << "ERROR GETTING FRONT POSITON" << endl;
        throw exception();
    }

    // Make sure that return the new pair
    return pair<int, int>(new_row, new_col);
}

Creature* Darwin::getFrontCreature(int row, int col) {
    pair<int, int> front_pos = getFrontPosition(row, col);
    int new_row = front_pos.first;
    int new_col = front_pos.second;

    // Because we are not ensured to be at a valid space in our grid we need to double
    // check.
    if(new_row < 0 || new_row >= (int)_grid.size()) {
        return nullptr;
    }
    if(new_col < 0 || new_col >= (int)_grid[0].size()) {
        return nullptr;
    }

    // If we get here then we are at a valid place in the grid, so just return what's there
    return _grid[new_row][new_col];
}

bool Darwin::isFacingWall(int row, int col) {
    pair<int, int> front_pos = getFrontPosition(row, col);
    int new_row = front_pos.first;
    int new_col = front_pos.second;

    // Now we actually want to check if we are in we are "in the walls"
    if(new_row < 0 || new_row >= (int)_grid.size()) {
        return true;
    }
    if(new_col < 0 || new_col >= (int)_grid[0].size()) {
        return true;
    }

    return false;
}

Darwin::Darwin(int row_len, int col_len) : _species(4), _grid(row_len, vector<Creature*>(col_len)) {
    if(DEBUG_DARWIN) {
        cout << "Row: " << _grid.size() << endl;
        cout << "Col: " << _grid[0].size() << endl;
    }

    // will make the species for this darwin object.
    createSpecies();
}

Species* Darwin::getSpecies(char type) {
    switch(type) {
    case 'f':
        return &_species[0];
    case 'h':
        return &_species[1];
    case 'r':
        return &_species[2];
    case 't':
        return &_species[3];
    default:
        cout << "ERROR IN Darwin::getSpecies()";
        throw exception();
    }
}

void Darwin::insertCreature(Creature* c, int row, int col) {
    // simply insert the creature to the creature array and insert
    // into the grid.
    _creature.push_back(c);
    _grid[row][col] = c;
}

void Darwin::printBoard() {
    for(int i = 0; i < (int)_grid.size(); ++i) {
        for(int j = 0; j < (int)_grid[i].size(); ++j) {
            cout << "_grid[" << i << "][" << j << "]: ";
            cout << _grid[i][j] << endl;
        }
    }
}

void Darwin::printState() {
    cout << "  ";
    for(int i = 0; i < (int)_grid[0].size(); ++i) {
        // Need to make sure that we just print out the ones digit
        cout << (i % 10);
    }
    cout << endl;

    for(int i = 0; i < (int)_grid.size(); ++i) {
        // Need to make sure that we just print out the ones digit
        cout << (i % 10) << " ";
        for(int j = 0; j < (int)_grid[i].size(); ++j) {
            Creature* cur_creature = _grid[i][j];
            if(!cur_creature) {
                cout << ".";
            } else {
                cur_creature->printSymbol();
            }
        }
        if(i != (int)_grid.size() - 1)
            cout << endl;
    }
}

void Darwin::oneIteration() {
    // here an optimization that was made was to make reseting the creatures
    // We do so by storing them in an internal creature array rather than going
    // through the entire grid looking for creatures.
    for(int i = 0; i < (int)_creature.size(); ++i) {
        Creature* cur_creature = _creature[i];
        cur_creature->resetVisited();
    }

    // Go through the grid, left to right, top to bottom, and execute each
    // creature once.

    // To do so we need to get some info before we can iterate.
    for(int i = 0; i < (int)_grid.size(); ++i) {
        for(int j = 0; j < (int)_grid[i].size(); ++j) {
            Creature* cur_creature = _grid[i][j];
            if(cur_creature && !cur_creature->getVisited()) {
                if(DEBUG_DARWIN) {
                    cout << "going to execute the following creature:" << endl;
                    cur_creature->printCreature();
                }
                // Construct information for creature to execute properly.
                // Needs creature in front, and if it is facing a wall
                Creature* in_front = getFrontCreature(i, j);
                bool facing_wall = isFacingWall(i, j);
                if(DEBUG_DARWIN) {
                    cout << "Creature in front: ";
                    cout << hex;
                    cout << in_front;
                    cout << dec;
                    cout << endl;
                    cout << "isFacingWall: " << facing_wall << endl;
                }
                // Because execute will only return true on hop
                // due to hop being the only instruction that modifies the board
                // that's the only one that warrents a response on the darwin level.
                bool hop = cur_creature->execute(in_front, facing_wall);
                if(DEBUG_DARWIN) {
                    cout << "finished execute" << endl;
                }

                if(hop) {
                    pair<int, int> front_pos = getFrontPosition(i, j);
                    _grid[i][j] = nullptr;
                    _grid[front_pos.first][front_pos.second] = cur_creature;
                }
                cur_creature->setVisited();
            }
        }
    }
}